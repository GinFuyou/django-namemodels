from django.db import models
from django.utils.translation import gettext_lazy as _
from .abstract import AbstractName, AbstractNameComposition


class Name(AbstractName):
    class Meta:
        db_table = 'djnm_names'
        verbose_name = _("name")
        verbose_name_plural = _("names")

    origin_mapping =  {'russian': 'ru',
                       'japanese': 'ja',
                       'german': 'de',
                       'chinese': 'zh',
                       'korean': 'ko'}
    
    origin_values = origin_mapping.keys()

    languages = origin_mapping.values()

    # translations
    zh = models.CharField(max_length=1024, blank=True)
    ko = models.CharField(max_length=1024, blank=True)
    ru = models.CharField(max_length=1024, blank=True)
    de = models.CharField(max_length=1024, blank=True)
    ja = models.CharField(max_length=1024, blank=True)

    is_original = models.BooleanField(default=False)

    # denormalized usages count
    total_count = models.PositiveIntegerField(default=0)
    
    def __str__(self):
        origin = self.origin.lower()
        original_name = ''
        if origin in self.origin_values:
             original_name = getattr(self, self.origin_mapping[origin], '')
        if original_name:
            return f"{self.main} [{original_name}]"
        else:
            return self.main
                


class NameComposition(AbstractNameComposition):
    class Meta(AbstractNameComposition.Meta):
        db_table = 'djnm_name_compositions'
        verbose_name = _("name composition")
        verbose_name_plural = _("name compositions")

    pass
