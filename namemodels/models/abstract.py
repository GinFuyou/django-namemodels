from django.db import models, transaction
from django.utils import timezone
from django.conf import settings
from django.apps import apps

from django.urls import reverse


SWAPPABLE_NAME_MODEL = getattr(settings, 'DJANGONAMES_NAMEMODEL', 'namemodels.Name')
SWAPPABLE_NAMECOMP_MODEL = getattr(settings, 'DJANGONAMES_NAMECOMPMODEL', 'namemodels.NameComposition')


class AbstractBaseNamedEntity(models.Model):
    class Meta:
        abstract = True

    main_name = models.ForeignKey(SWAPPABLE_NAME_MODEL, related_name='main_usages', on_delete=models.PROTECT)

    name_pattern = models.CharField(max_length=1024, blank=True)  # NOTE probably unneeded


class AbstractNamedEntity(AbstractBaseNamedEntity):
    class Meta:
        abstract = True

    last_name = models.ForeignKey(SWAPPABLE_NAME_MODEL, blank=True, null=True, related_name='last_usages', on_delete=models.PROTECT)
    middle_name = models.ForeignKey(SWAPPABLE_NAME_MODEL, blank=True, null=True, related_name='middle_usages', on_delete=models.PROTECT)
    # other names can be extracted but full name\titles can't be parsed to known scheme
    extended_name = models.ForeignKey(SWAPPABLE_NAME_MODEL, blank=True, null=True, related_name='extended_usages', on_delete=models.PROTECT)

    use_short_name = models.BooleanField(default=False)
    short_main_name = models.ForeignKey(SWAPPABLE_NAME_MODEL, blank=True, null=True, related_name='short_main_usages', on_delete=models.PROTECT)
    short_last_name = models.ForeignKey(SWAPPABLE_NAME_MODEL, blank=True, null=True, related_name='short_last_usages', on_delete=models.PROTECT)

    def get_base_name(self, connector=" "):
        if self.use_short_name:
            name_str = self.short_main_name.main
            if self.short_last_name:
                name_str += connector + self.short_last_name.main
        else:
            name_str = self.get_full_name(connector=connector)
        return name_str

    def get_full_name(self, connector=" "):
        names = []
        if self.main_name:
            names.append(self.main_name)
        if self.middle_name:
            names.append(self.middle_name)
        if self.last_name:
            names.append(self.last_name)
        return connector.join([name.main for name in names])

    def set_name_from_list(self, name_list, target='main_name', language=''):
        assert target in ('main_name', 'middle_name', 'last_name')
        length = len(name_list)
        NameClass = apps.get_model(SWAPPABLE_NAME_MODEL)

        if length == 1:
            name, created = NameClass.objects.get_or_create(**name_list[0])
            setattr(self, target, name)
        elif length > 1:
            # get_or_create_composite(self, name_str_list, language='', kwargs={'is_surname': True})
            kwargs = {"is_surname": False, "is_particle": False, "is_genonymic": False}
            if target == "middle_name":
                kwargs['is_genonymic'] = True  # TODO add separate is_genonymic input
            elif target == "last_name":
                kwargs['is_surname'] = True

            name, created = NameClass.objects.get_or_create_composite(name_list, language=language, kwargs=kwargs)
            setattr(self, target, name)
        else:
            name = None

        if length and self.use_short_name:
            if target == "main_name":
                # select first non-particle
                for name_dict in name_list:
                    if not name_dict['is_particle']:
                        self.short_main_name = NameClass.objects.get_or_create(**name_dict)[0]
                        break
            elif target == "last_name":
                self.short_last_name = NameClass.objects.get_or_create(**name_list[-1])[0]
        return name

    def get_admin_url(self):
        return reverse(f'admin:{self._meta.app_label}_{self._meta.model_name}_change', args=(self.pk, ))

class NameQuerySet(models.QuerySet):
    def origin_choices(self):
        return self.annotate(origin2=models.F('origin')).distinct('origin').values_list('origin', 'origin2')

    def annotate_usages(self):
        return self.annotate(main_count=models.Count('main_usages'),
                             last_count=models.Count('last_usages'),
                             total_count2=models.F('last_count') + models.F('main_count'))

    def annotate_variants(self):
        return self.annotate(variant_count=models.Count('variants'))

    def marked_string_to_dicts(self, marked_string, marked_origin_string="", origin="", language=""):
        mark_dict = {'@': {'is_surname': False},
                     '+': {'is_particle': True},
                     '#': {'is_surname': True},
                     '^': {'is_genonymic': True}}
        base_dict = {'is_surname': False, 'is_particle': False, 'is_genonymic': False}
        splits = marked_string.split(" ")
        if marked_origin_string:
            origin_splits = marked_origin_string.split(" ")
        else:
            origin_splits = None

        first_names_list = []
        last_names_list = []
        mid_names_list = []
        tmp_particles = []  # keep particles for later use
        counter = 0
        for marked_part in splits:
            mark = marked_part[0]
            part = marked_part[1:]
            namedict = base_dict.copy()
            namedict.update(mark_dict[mark])
            namedict['main'] = part
            if origin_splits and language:
                namedict[language] = origin_splits[counter][1:]
            if origin:
                namedict['origin'] = origin
           
            if mark == "+":
                # particles
                tmp_particles.append(namedict)
            else:
                 # if there are unused particles, put them before current place
                if tmp_particles:
                    appendix = tmp_particles.copy()
                    appendix.append(namedict)
                    tmp_particles = []
                else:
                    appendix = [namedict,]

                if mark == "@":
                    first_names_list += appendix
                elif mark =='^':
                    mid_names_list += appendix
                elif mark =='#':
                    last_names_list += appendix
                
                
            counter += 1
        return (first_names_list, mid_names_list, last_names_list)

    def get_or_create_composite(self, name_str_list, language='', kwargs={'is_surname': True}):
        #composite_created = False
        name_obj_list = []
        with transaction.atomic():
            for name_dict in name_str_list:
                name = self.model.objects.get_or_create(**name_dict)[0]
                name_obj_list.append(name)
            kwargs['main'] = " ".join([name.main for name in name_obj_list])
            if language:
                kwargs[language] = " ".join([getattr(name, language) for name in name_obj_list])
            composite_name, created = self.model.objects.get_or_create(is_composite=True, **kwargs)
            if created:
                NameCompositionClass = apps.get_model(SWAPPABLE_NAMECOMP_MODEL)
                compositions = []
                counter = 1
                for name in name_obj_list:
                    compositions.append(NameCompositionClass(part=name, composed=composite_name, position=counter))
                    counter += 1
                NameCompositionClass.objects.bulk_create(compositions)
        # END atomic
        return composite_name, created

class AbstractName(models.Model):
    class Meta:
        abstract = True

    # main name reprsentation in [a-ZA-Z0-9\-\'\ ]+
    main = models.CharField(max_length=1024, db_index=True)

    # name is of unknown format, not fitting any know pattern or not known to be first\last\middle
    unknown_scheme = models.BooleanField(default=False)

    # is not really a name but a particle, which is also translatable
    is_particle = models.BooleanField(default=False)

    # name is composed of other eligible Names
    is_composite = models.BooleanField(default=False)

    is_feminine = models.BooleanField(default=False)
    is_masculine = models.BooleanField(default=False)

    is_surname = models.BooleanField(default=False)
    is_genonymic = models.BooleanField(default=False)

    # origin of name
    origin = models.CharField(max_length=256, blank=True)

    note = models.TextField(blank=True)

    variants = models.ManyToManyField("self", blank=True, db_table='djnm_name_variants')

    update_dt = models.DateTimeField(default=timezone.localtime)

    objects = NameQuerySet.as_manager()

    def __str__(self):
        return self.main

class AbstractNameComposition(models.Model):
    class Meta:
        abstract = True
        unique_together = (('part', 'composed'), ('composed', 'position'))

    part = models.ForeignKey(SWAPPABLE_NAME_MODEL, related_name='composition_parts', on_delete=models.PROTECT)
    composed = models.ForeignKey(SWAPPABLE_NAME_MODEL, related_name='compositions', on_delete=models.PROTECT)
    position = models.PositiveSmallIntegerField(default=1)
    connector = models.CharField(max_length=128, blank=True)

    def __str__(self):
        return "{0.part.main} -> {0.composed.main}".format(self)
