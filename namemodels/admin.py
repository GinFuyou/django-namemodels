from django.contrib import admin, messages
from django.db import transaction, models
from django.utils.html import format_html

from .models import Name, NameComposition


# NOTE MOVE IT
def get_reverse_foreignkeys(model):
    return [f for f in model._meta.get_fields() if isinstance(f, models.ManyToOneRel)]


def get_m2m_relations(model):
    return [f for f in model._meta.get_fields() if isinstance(f, models.ManyToManyRel)]


def get_m2m_fields(model):
    return [f for f in model._meta.get_fields() if isinstance(f, models.ManyToManyField)]

"""
def transfer_unique_together_collisions(target, source, relation_field, unique_together_fields):
    from django.db import models
    source_manager = getattr(source, relation_field.get_accessor_name())
    target_manager = getattr(target, relation_field.get_accessor_name())
    source_values = list(source_manager.values(*unique_together_fields))
    if not len(source_values):  # no chance of collision
        return
    lookups = reduce(operator.or_, [models.Q(**x) for x in source_values])
    for duplicate_target in target_manager.filter(lookups):
        lookup = {x: getattr(duplicate_target, x) for x in unique_together_fields}
        duplicate_source = source_manager.get(**lookup)
        transfer_relations(duplicate_target, duplicate_source)
        duplicate_source.delete()
"""

@transaction.atomic
def transfer_relations(target, source, commit=True, m2m=True):
    """Moves all reverse relations from source to target"""
    # Please note this implementation may or may not work with all models (definitely doesn't work with User)
    if target._meta.model != source._meta.model:
        raise ValueError("Relations can be transferred only between instances of the same model")

    reverse_foreignkeys = get_reverse_foreignkeys(target)
    for f in reverse_foreignkeys:
        remote_name = f.remote_field.name
        print(f"Remote name: {remote_name}")

        if isinstance(f, models.OneToOneRel):
            # At the moment, we don't have 1to1 relations that we really need to transfer but, if that should happen,
            # we should also figure out a way of dealing with the inevitable collisions
            continue

        manager = getattr(source, f.get_accessor_name())
        if commit:
            manager.update(**{f.remote_field.name: target})

    """
    for gfk_update in get_generic_relations_to_model(target):
        try:
            gfk_update['queryset'].filter(**{gfk_update['field']: source.pk}).update(**{gfk_update['field']: target.pk})
        except (BulkUpdateNotAllowed, ValueError):  # if the pk can't be cast as the object_id we get ValueError
            pass
    """

    if m2m:

        for m2m in get_m2m_relations(target):
            source_rel = getattr(source, m2m.get_accessor_name())
            target_rel = getattr(target, m2m.get_accessor_name())
            target_rel.add(*source_rel.all())
            source_rel.clear()

        for m2m in get_m2m_fields(target):
            source_rel = getattr(source, m2m.name)
            target_rel = getattr(target, m2m.name)
            target_rel.add(*source_rel.all())
            source_rel.clear()

    return target
# END


class GenderListFilter(admin.SimpleListFilter):
    title = 'Gender usage'

    parameter_name = 'g'

    def lookups(self, request, model_admin):
        return (('f','feminine'),
                ('m', 'masculine'),
                ('fm', 'unisex'),
                ('o', 'unknown'))

    def queryset(self, request, queryset):
        """
        Returns the filtered queryset based on the value
        provided in the query string and retrievable via
        `self.value()`.
        """
        val = self.value()
        if self.value() is None:
            return queryset.all()
        else:
            f = {}
            if val == 'o':
                f = {'is_masculine': False, 'is_feminine': False}
            else:
                if 'f' in val:
                    f = {'is_feminine': True}
                if 'm' in val:
                    f = {'is_masculine': True}
            return queryset.filter(**f)


@admin.register(Name)
class NameAdmin(admin.ModelAdmin):
    def get_main(self, obj):
        return str(obj)

    def set_japanese(self, request, queryset):
        count = queryset.filter(origin__in=('Unknown', '')).update(origin="Japanese")
        self.message_user(request, f"Set origin on {count} name(s)", level=messages.INFO)


    def merge_names(self, request, queryset):
        obj = queryset[0]
        Q = models.Q

        # deny names that are composite or parts of composite
        composite_qs = queryset.filter(Q(is_composite=True)
                                       | Q(composition_parts__isnull=False)
                                       | Q(compositions__isnull=False))
        if composite_qs:
            self.message_user(request,
                              f"Operation forbidden on composite names or parts:"
                              f" {', '.join([n.main for n in composite_qs])}",
                              level=messages.ERROR)
            return

        # deny names with variants:
        variant_qs = queryset.filter(variant_count__gt=0)
        if variant_qs:
            self.message_user(request,
                              f"Operation forbidden on names with variants:"
                              f" {', '.join([f'{n.main} #{n.pk}' for n in variant_qs])}",
                              level=messages.ERROR)
            return

        # only one target should have good origin
        target_qs = queryset.exclude(origin__in=("", "Unknown"))
        count = len(target_qs)
        if count != 1:
            self.message_user(request,
                              f"Must have exactly one target with 'good' origin",
                              level=messages.ERROR)
            return

        target = target_qs.get()
        source_qs = queryset.exclude(pk=target.pk)

        fk_fields = get_reverse_foreignkeys(obj)
        m2m_fields = get_m2m_fields(obj)
        m2m_rels = get_m2m_relations(obj)

        self.message_user(request, "Foreign fields to merge:", level=messages.WARNING)
        for field in fk_fields:
            self.message_user(request, f"{field.name}", level=messages.WARNING)

        self.message_user(request, "M2M fields to merge:", level=messages.WARNING)
        for field in m2m_fields:
            self.message_user(request, f"{field.name}", level=messages.WARNING)
        for field in m2m_rels:
            self.message_user(request, f"{field.name}", level=messages.WARNING)

        with transaction.atomic():
            for source_name in source_qs:
                transfer_relations(target, source_name, commit=True, m2m=False)
                target.full_clean()
                source_name.delete()
        self.message_user(request, "Finished", level=messages.INFO)

    @admin.display(ordering='total_count2')
    def get_count(self, obj):
        return f"{obj.main_count}/{obj.last_count}"

    @admin.display(ordering='variant_count')
    def get_variants(self, obj):
        if obj.variant_count:
            return obj.variant_count
        else:
            return ""

    @admin.display()
    def get_translations(self, obj):
        translations = []
        for field_name in obj.origin_mapping.values():
            val = getattr(obj, field_name)
            if val:
                translations.append(val)
        return format_html("<small>{0}</small>", ", ".join(translations))


    get_count.short_description = "usages"

    get_main.admin_order_field = 'main'
    get_main.short_description = 'Main'

    list_display = ('get_main', 'get_translations', 'is_surname',
                    'is_genonymic', 'get_variants',
                    'origin', 'get_count')
    search_fields = ('main', 'ru', 'ja', 'de', 'zh', 'ko')
    autocomplete_fields = ('variants', )
    list_filter = (GenderListFilter, 'is_surname', 'is_genonymic',
                   'is_composite', 'origin')

    actions = [merge_names, set_japanese]

    fields = ('main',
              ('is_composite', 'is_particle'),
              ('is_surname', 'is_genonymic'),
              ('is_feminine', 'is_masculine'),
              ('origin', 'is_original'),
              'variants',
              ('ja', 'zh'),
              ('ko', 'de'),
              'ru',
              'total_count',
              'note',
              'update_dt')

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        qs = qs.annotate_usages().annotate_variants()
        return qs

@admin.register(NameComposition)
class NameCompositionAdmin(admin.ModelAdmin):
    list_display = (str, 'connector', 'position')
