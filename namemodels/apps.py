from django.apps import AppConfig


class NamemodelsConfig(AppConfig):
    name = 'namemodels'
